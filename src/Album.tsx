import { Badge, Box, Image, Text } from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { Album as AlbumType, User } from "./types"

type Props = {
  album: AlbumType
  user: User | undefined
}

const Album = ({ album, user }: Props) => {
  const [photo, setPhoto] = useState("")

  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/albums/${album.id}/photos`)
      .then((res) => res.json())
      .then((result) => {
        setPhoto(result[0].thumbnailUrl)
      })
  }, [album.id])

  return (
    <Box w="100%" borderWidth="1px">
      <Image src={photo} alt="Album ThumbNail" w="100%" />
      <Box p="5" w="100%">
        <Text>{album.title}</Text>
        {user && (
          <Link to={`/users/${user.username}`}>
            <Badge>By {user.name}</Badge>
          </Link>
        )}
      </Box>
    </Box>
  )
}

export default Album
