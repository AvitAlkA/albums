import { BrowserRouter as Router, Switch, Route } from "react-router-dom"
import Albums from "./Albums"
import UserPage from "./UserPage"

function App() {
  return (
    <Router>
      <div className="App">
        <header className="App-header" />
        <Switch>
          <Route exact path="/">
            <Albums />
          </Route>
          <Route path="/users/:username">
            <UserPage />
          </Route>
        </Switch>
      </div>
    </Router>
  )
}

export default App
