import { Grid } from "@chakra-ui/react"
import React, { useEffect, useState } from "react"
import Album from "./Album"
import { Album as AlbumType, User } from "./types"

function shuffleArray(array: any[]): any[] {
  const newArray = [...array]
  for (let i = array.length - 1; i > 0; i -= 1) {
    const j = Math.floor(Math.random() * (i + 1))
    ;[newArray[i], newArray[j]] = [newArray[j], newArray[i]]
  }
  return newArray
}

const Albums = () => {
  const [albums, setAlbums] = useState<AlbumType[]>([])
  const [users, setUsers] = useState<User[]>([])

  useEffect(() => {
    fetch("https://jsonplaceholder.typicode.com/albums")
      .then((res) => res.json())
      .then((result: AlbumType[]) => {
        setAlbums(shuffleArray(result))
      })

    fetch("https://jsonplaceholder.typicode.com/users")
      .then((res) => res.json())
      .then((result: User[]) => {
        setUsers(result)
      })
  }, [])

  return (
    <Grid
      p={5}
      templateColumns={["1fr", "repeat(auto-fill, 300px)"]}
      gap={4}
      justifyContent="center"
    >
      {albums.map((album) => (
        <Album
          key={album.id}
          album={album}
          user={users.find((user) => user.id === album.userId)}
        />
      ))}
    </Grid>
  )
}

export default Albums
