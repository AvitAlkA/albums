import {
  Box,
  Grid,
  GridItem,
  Heading,
  Circle,
  Flex,
  Text,
  Container,
  useBreakpointValue,
} from "@chakra-ui/react"
import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { User } from "./types"

type UserPageParameters = {
  username: string
}

const UserPage = () => {
  const [user, setUser] = useState<User>()
  const { username } = useParams<UserPageParameters>()

  useEffect(() => {
    fetch(`https://jsonplaceholder.typicode.com/users?username=${username}`)
      .then((res) => res.json())
      .then(([result]: [User]) => {
        setUser(result)
      })
  }, [username])

  const imageSize = useBreakpointValue({ base: "64px", md: "115px" })

  if (!user) return <div>Loading...</div>

  return (
    <Container p="3" maxW="container.lg">
      <Flex alignItems="center" mb="6">
        <Circle
          w={imageSize}
          h={imageSize}
          bgGradient="linear(to-r, green.200, pink.500)"
          mr="4"
        />
        <Box>
          <Heading as="h2" size="lg">
            {user.name}
          </Heading>
          <Text color="gray">@{user.username}</Text>
        </Box>
      </Flex>
      <Box>
        <Text>{user.email}</Text>
        <Text>{user.phone}</Text>
      </Box>
      <Grid
        h="200px"
        templateRows="repeat(2, 1fr)"
        templateColumns="repeat(5, 1fr)"
        gap={4}
      >
        <GridItem rowSpan={2} colSpan={1} bg="tomato" />
        <GridItem colSpan={2} bg="papayawhip" />
        <GridItem colSpan={2} bg="papayawhip" />
        <GridItem colSpan={4} bg="tomato" />
      </Grid>
    </Container>
  )
}

export default UserPage
